/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package session;

import entity.AdminAccount;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author viquy
 */
@Stateless
public class AdminAccountFacade extends AbstractFacade<AdminAccount> {

    @PersistenceContext(unitName = "LibraryManagerPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public AdminAccountFacade() {
        super(AdminAccount.class);
    }
    
}
