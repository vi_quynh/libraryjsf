/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author viquy
 */
@Entity
@Table(name = "Producer")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Producer.findAll", query = "SELECT p FROM Producer p")
    , @NamedQuery(name = "Producer.findByProducerID", query = "SELECT p FROM Producer p WHERE p.producerID = :producerID")
    , @NamedQuery(name = "Producer.findByProducerName", query = "SELECT p FROM Producer p WHERE p.producerName = :producerName")
    , @NamedQuery(name = "Producer.findByProducerAddress", query = "SELECT p FROM Producer p WHERE p.producerAddress = :producerAddress")
    , @NamedQuery(name = "Producer.findByProducerPhone", query = "SELECT p FROM Producer p WHERE p.producerPhone = :producerPhone")
    , @NamedQuery(name = "Producer.findByProducerEmail", query = "SELECT p FROM Producer p WHERE p.producerEmail = :producerEmail")})
public class Producer implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 250)
    @Column(name = "producerID")
    private String producerID;
    @Size(max = 250)
    @Column(name = "producerName")
    private String producerName;
    @Size(max = 250)
    @Column(name = "producerAddress")
    private String producerAddress;
    @Size(max = 20)
    @Column(name = "producerPhone")
    private String producerPhone;
    @Size(max = 250)
    @Column(name = "producerEmail")
    private String producerEmail;
    @OneToMany(mappedBy = "producerID")
    private List<Book> bookList;

    public Producer() {
    }

    public Producer(String producerID) {
        this.producerID = producerID;
    }

    public String getProducerID() {
        return producerID;
    }

    public void setProducerID(String producerID) {
        this.producerID = producerID;
    }

    public String getProducerName() {
        return producerName;
    }

    public void setProducerName(String producerName) {
        this.producerName = producerName;
    }

    public String getProducerAddress() {
        return producerAddress;
    }

    public void setProducerAddress(String producerAddress) {
        this.producerAddress = producerAddress;
    }

    public String getProducerPhone() {
        return producerPhone;
    }

    public void setProducerPhone(String producerPhone) {
        this.producerPhone = producerPhone;
    }

    public String getProducerEmail() {
        return producerEmail;
    }

    public void setProducerEmail(String producerEmail) {
        this.producerEmail = producerEmail;
    }

    @XmlTransient
    public List<Book> getBookList() {
        return bookList;
    }

    public void setBookList(List<Book> bookList) {
        this.bookList = bookList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (producerID != null ? producerID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Producer)) {
            return false;
        }
        Producer other = (Producer) object;
        if ((this.producerID == null && other.producerID != null) || (this.producerID != null && !this.producerID.equals(other.producerID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.Producer[ producerID=" + producerID + " ]";
    }
    
}
