/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author viquy
 */
@Entity
@Table(name = "AdminAccount")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "AdminAccount.findAll", query = "SELECT a FROM AdminAccount a")
    , @NamedQuery(name = "AdminAccount.findById", query = "SELECT a FROM AdminAccount a WHERE a.id = :id")
    , @NamedQuery(name = "AdminAccount.findByUsername", query = "SELECT a FROM AdminAccount a WHERE a.username = :username")
    , @NamedQuery(name = "AdminAccount.findByPassword", query = "SELECT a FROM AdminAccount a WHERE a.password = :password")})
public class AdminAccount implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "id")
    private Integer id;
    @Size(max = 50)
    @Column(name = "username")
    private String username;
    @Size(max = 50)
    @Column(name = "password")
    private String password;

    public AdminAccount() {
    }

    public AdminAccount(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AdminAccount)) {
            return false;
        }
        AdminAccount other = (AdminAccount) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.AdminAccount[ id=" + id + " ]";
    }
    
}
