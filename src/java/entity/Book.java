/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author viquy
 */
@Entity
@Table(name = "Book")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Book.findAll", query = "SELECT b FROM Book b")
    , @NamedQuery(name = "Book.findByBookID", query = "SELECT b FROM Book b WHERE b.bookID = :bookID")
    , @NamedQuery(name = "Book.findByBookName", query = "SELECT b FROM Book b WHERE b.bookName = :bookName")
    , @NamedQuery(name = "Book.findByBookDes", query = "SELECT b FROM Book b WHERE b.bookDes = :bookDes")
    , @NamedQuery(name = "Book.findByBookIMG", query = "SELECT b FROM Book b WHERE b.bookIMG = :bookIMG")
    , @NamedQuery(name = "Book.findByBookfile", query = "SELECT b FROM Book b WHERE b.bookfile = :bookfile")
    , @NamedQuery(name = "Book.findByBookstatus", query = "SELECT b FROM Book b WHERE b.bookstatus = :bookstatus")})
public class Book implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 250)
    @Column(name = "bookID")
    private String bookID;
    @Size(max = 250)
    @Column(name = "bookName")
    private String bookName;
    @Size(max = 250)
    @Column(name = "bookDes")
    private String bookDes;
    @Size(max = 250)
    @Column(name = "bookIMG")
    private String bookIMG;
    @Size(max = 250)
    @Column(name = "bookfile")
    private String bookfile;
    @Size(max = 1)
    @Column(name = "bookstatus")
    private String bookstatus;
    @JoinColumn(name = "authorID", referencedColumnName = "authorID")
    @ManyToOne
    private Author authorID;
    @JoinColumn(name = "categoryID", referencedColumnName = "categoryID")
    @ManyToOne
    private Category categoryID;
    @JoinColumn(name = "producerID", referencedColumnName = "producerID")
    @ManyToOne
    private Producer producerID;

    public Book() {
    }

    public Book(String bookID) {
        this.bookID = bookID;
    }

    public String getBookID() {
        return bookID;
    }

    public void setBookID(String bookID) {
        this.bookID = bookID;
    }

    public String getBookName() {
        return bookName;
    }

    public void setBookName(String bookName) {
        this.bookName = bookName;
    }

    public String getBookDes() {
        return bookDes;
    }

    public void setBookDes(String bookDes) {
        this.bookDes = bookDes;
    }

    public String getBookIMG() {
        return bookIMG;
    }

    public void setBookIMG(String bookIMG) {
        this.bookIMG = bookIMG;
    }

    public String getBookfile() {
        return bookfile;
    }

    public void setBookfile(String bookfile) {
        this.bookfile = bookfile;
    }

    public String getBookstatus() {
        return bookstatus;
    }

    public void setBookstatus(String bookstatus) {
        this.bookstatus = bookstatus;
    }

    public Author getAuthorID() {
        return authorID;
    }

    public void setAuthorID(Author authorID) {
        this.authorID = authorID;
    }

    public Category getCategoryID() {
        return categoryID;
    }

    public void setCategoryID(Category categoryID) {
        this.categoryID = categoryID;
    }

    public Producer getProducerID() {
        return producerID;
    }

    public void setProducerID(Producer producerID) {
        this.producerID = producerID;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (bookID != null ? bookID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Book)) {
            return false;
        }
        Book other = (Book) object;
        if ((this.bookID == null && other.bookID != null) || (this.bookID != null && !this.bookID.equals(other.bookID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.Book[ bookID=" + bookID + " ]";
    }
    
}
